node[:deploy].each do |application, deploy|
  app_root = "#{deploy[:deploy_to]}/current"
  app_root << "/#{deploy[:document_root]}" unless deploy[:document_root].nil? || deploy[:document_root].empty?
  uploads_dir = "#{deploy[:deploy_to]}/shared/uploads"
  group = "www-data"
  group = "apache" unless `getent group apache`.empty?
  bash "fix_wp_uploads" do
    code <<-EOH
      if ! [ -d "#{uploads_dir}" ]; then
        mkdir "#{uploads_dir}"
      fi

      chown deploy:#{group} #{uploads_dir}
      chmod 0777 #{uploads_dir}

      if [ -d "#{app_root}/wp-content/uploads" ]; then
        rsync -a "#{app_root}/wp-content/uploads/" #{uploads_dir}/
        rm -rf "#{app_root}/wp-content/uploads"
      fi

      find #{uploads_dir} -type d -exec chmod 777 {} \\;
      find #{uploads_dir} -type f -exec chmod g+w {} \\;

      ln -s "#{uploads_dir}" "#{app_root}/wp-content/uploads"
      chown -R deploy:#{group} "#{app_root}/wp-content/uploads"

      EOH
    only_if { File.directory?("#{app_root}/wp-content") }
  end
end
