
node[:deploy].each do |app_name, deploy|
    apache_conf = "/etc/apache2/apache2.conf"
    php_conf = "/etc/php5/apache2/php.ini"
    ruby_block "add options to the apache config" do
        block do
            fe = Chef::Util::FileEdit.new(apache_conf)

            fe.insert_line_if_no_match("  Header always append X-Frame-Options SAMEORIGIN", "<IfModule headers_module>\n  Header always append X-Frame-Options SAMEORIGIN\n  Header always append X-XSS-Protection 1\n</IfModule>\n")
            fe.write_file

            # Insert some options at the end of the file
            fe.insert_line_if_no_match("TraceEnable Off", "TraceEnable Off")
            fe.write_file
            fe.insert_line_if_no_match("ServerSignature Off", "ServerSignature Off")
            fe.write_file
            fe.insert_line_if_no_match("ServerTokens ProductOnly", "ServerTokens ProductOnly")
            fe.write_file
        end
    end

    ruby_block "add options to the php config" do
      block do
        fe = Chef::Util::FileEdit.new(php_conf)
        fe.search_file_replace("expose_php = On", "expose_php = Off")
        fe.search_file_replace("upload_max_filesize = 32M", "upload_max_filesize = 64M")
        fe.search_file_replace("short_open_tag = On", "short_open_tag = Off")
        fe.search_file_replace("max_execution_time = 30", "max_execution_time = 120")
        #file = File.read('http://www.ec2instances.info/instances.json')
        #data_hash = JSON.parse(file)
        # Search in data_hash["instance_type"] for node[:deploy]["instance"]["instance_type"]
        fe.search_file_replace("memory_limit = 128M", "memory_limit = 128M")
        # Check if instance has WebAdmin security group?
        fe.search_file_replace("post_max_size = 32M", "post_max_size = 64M")
        fe.search_file_replace("upload_max_filesize = 32M", "upload_max_filesize = 64M")
        fe.write_file
      end
    end
end
