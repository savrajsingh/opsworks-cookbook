node[:deploy].each do |application, deploy|
  app_root = "#{deploy[:deploy_to]}/current"
  app_root << "/#{deploy[:document_root]}" unless deploy[:document_root].nil? || deploy[:document_root].empty?
  bash "set_w3tc_permissions" do
    user 'root'
    cwd "#{app_root}/wp-content"
    code <<-EOH
      mkdir -p w3tc-config
      mkdir -p cache/{config,object}
      chmod -R 0777 w3tc-config/ cache/
      chown -R deploy:www-data w3tc-config/ cache/
      EOH
    only_if { File.directory?("#{app_root}/wp-content/plugins/w3-total-cache") }
  end
end
# THIS IS OLD, USE beta_wp_uploads::fix!
