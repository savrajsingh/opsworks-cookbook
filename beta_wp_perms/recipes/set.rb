node[:deploy].each do |application, deploy|
  app_root = "#{deploy[:deploy_to]}/current"
  app_root << "/#{deploy[:document_root]}" unless deploy[:document_root].nil? || deploy[:document_root].empty?
  bash "set_uploads_permissions" do
    user 'root'
    cwd "#{app_root}/wp-content"
    code <<-EOH
      mkdir uploads
      chmod -R 0777 uploads/
      chown -R deploy:www-data uploads/
      EOH
    only_if { File.directory?("#{app_root}/wp-content") }
  end
  # For W3TC perms, check beta_w3tc_perms::fix
end
# THIS IS OLD, USE beta_wp_uploads::fix!
