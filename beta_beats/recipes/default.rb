node[:deploy].each do |application, deploy|
    bash "add_elastic_source" do
        user 'root'
        code <<-EOH
            echo "deb https://packages.elastic.co/beats/apt stable main" | tee -a /etc/apt/sources.list.d/beats.list
            wget -qO - https://packages.elastic.co/GPG-KEY-elasticsearch | apt-key add -
            apt-get update
            mkdir -p /etc/pki/tls/certs
        EOH
    end

    cookbook_file '/etc/pki/tls/certs/logstash-forwarder.crt' do
        source 'logstash-forwarder.crt'
        owner 'root'
        group 'root'
        mode '0600'
        action :create
    end

    # Remove files so we don't get an error on installation / upgrade
    file '/etc/filebeat/filebeat.yml' do
        action :delete
    end

    file '/etc/topbeat/topbeat.yml' do
        action :delete
    end

    apt_package 'filebeat'  do
        action :install
    end

    apt_package 'topbeat'  do
        action :install
    end

    cookbook_file '/etc/filebeat/elk.yml' do
        source 'filebeat.yml'
        owner 'root'
        group 'root'
        mode '0600'
        action :create
    end

    cookbook_file '/etc/topbeat/elk.yml' do
        source 'topbeat.yml'
        owner 'root'
        group 'root'
        mode '0600'
        action :create
    end

    ruby_block "use custom ylm config for filebeat" do
        block do
            fe = Chef::Util::FileEdit.new('/etc/init.d/filebeat')
            fe.search_file_replace(/filebeat\.yml/, 'elk.yml')
            fe.write_file
        end
    end

    ruby_block "use custom ylm config for topbeat" do
        block do
            fe = Chef::Util::FileEdit.new('/etc/init.d/topbeat')
            fe.search_file_replace(/topbeat\.yml/, 'elk.yml')
            fe.write_file
        end
    end

    bash "update_rc_filebeat" do
        user 'root'
        code <<-EOH
            service filebeat restart
            update-rc.d filebeat defaults 95 10
        EOH
    end

    bash "update_rc_topbeat" do
        user 'root'
        code <<-EOH
            service topbeat restart
            update-rc.d topbeat defaults 95 10
        EOH
    end
end
