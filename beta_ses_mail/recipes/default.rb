# Required JSON for this to work:
# {
# "beta_ses_mail": {
#   "domain": DOMAIN.COM,
#   "ses": {
#     "endpoint": "",
#     "user": "",
#     "password": ""
#     }
#   }
# }
# You have to populate that with the SES endpoint and its SMTP User and Password
# as well as the domain you want to use to send mail from (which must be configured
# and validated in SES as well)
node[:deploy].each do |application, deploy|
    apt_package 'sendmail'  do
        action :install
    end

    bash "add ses stuff to sendmail.mc" do
        user 'root'
        code <<-EOH
            sed -i "/Default Mailer setup$/a FEATURE('authinfo', 'hash -o /etc/mail/authinfo.db')dnl\\\ndefine('SMART_HOST', '[127.0.0.1]')dnl\\\ndefine('RELAY_MAILER_ARGS', 'TCP $h 2525')dnl\\\ndefine('ESMTP_MAILER_ARGS', 'TCP $h 2525')dnl\\\nMASQUERADE_AS('#{node['beta_ses_mail']['domain']}')dnl\\\nFEATURE(masquerade_envelope)dnl\\\nFEATURE(masquerade_entire_domain)dnl" /etc/mail/sendmail.mc
        EOH
    end

    bash "set up sendmail for ses" do
        user 'root'
        code <<-EOH
            echo 'AuthInfo:#{node['beta_ses_mail']['ses']['endpoint']} "U:root" "I:#{node['beta_ses_mail']['ses']['user']}" "P:#{node['beta_ses_mail']['ses']['password']}" "M:LOGIN"' > /etc/mail/authinfo
            makemap hash /etc/mail/authinfo.db < /etc/mail/authinfo
            echo "Connect:#{node['beta_ses_mail']['ses']['endpoint']} RELAY" >> /etc/mail/access
            makemap hash /etc/mail/access.db < /etc/mail/access
            chmod 666 /etc/mail/sendmail.cf
            m4 /etc/mail/sendmail.mc > /etc/mail/sendmail.cf
            chmod 644 /etc/mail/sendmail.cf
            service sendmail restart
        EOH
    end
end
