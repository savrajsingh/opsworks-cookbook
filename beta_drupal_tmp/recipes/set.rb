node[:deploy].each do |application, deploy|
  app_root = "#{deploy[:deploy_to]}/current"
  app_root << "/#{deploy[:document_root]}" unless deploy[:document_root].nil? || deploy[:document_root].empty?
  uploads_dir = "#{deploy[:deploy_to]}/shared/uploads"
  group = "www-data"
  group = "apache" unless `getent group apache`.empty?
  bash "Create upload directory and symlink shared uploads" do
    user 'root'
    cwd "#{app_root}/sites/default/files"
    code <<-EOH
      chmod 0777 "#{uploads_dir}"
      find #{uploads_dir} -type d -exec chmod 777 {} \\;
      find #{uploads_dir} -type f -exec chmod g+w {} \\;
    EOH
    only_if { File.directory?("#{app_root}/sites/default/files") }
  end
end
